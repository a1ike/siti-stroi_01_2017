$(document).ready(function() {

  $('.owl-modal').owlCarousel({
    navigation: true, // показывать кнопки next и prev 
    pagination: false,
    slideSpeed: 300,
    paginationSpeed: 400,

    items: 1,
    itemsDesktop: false,
    itemsDesktopSmall: false,
    itemsTablet: false,
    itemsMobile: false,
    navigationText: [
      '<img src=\'images/prev.png\'>',
      '<img src=\'images/next.png\'>'
    ]
  });

  $('#nav-icon').click(function() {
    $('.fullscreen-nav').toggleClass('open');
  });

  $('#close-icon').click(function() {
    $('.fullscreen-nav').toggleClass('open');
  });

  $('#alt-close-icon').click(function() {
    $('.fullscreen-nav').toggleClass('open');
  });

  $('#owl-sale').owlCarousel({

    navigation: true, // Show next and prev buttons
    slideSpeed: 300,
    pagination: true,
    paginationSpeed: 400,
    singleItem: true,
    navigationText: [
      '<img src=\'images/owl_left.png\'>',
      '<img src=\'images/owl_right.png\'>'
    ]

    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false

  });

  lightbox.option({
    'resizeDuration': 200,
    'wrapAround': true
  });

  // Price Slider
  if ($('#ex1').length > 0) {
    $('#ex1').bootstrapSlider({
      tooltip: 'hide',
      step: 10000,
    }).on('slide', function() {
      var val = $(this).bootstrapSlider('getValue');
      $('#ex1-1 input').val(val[0]);
      $('#ex1-2 input').val(val[1]);
    });
  }

  $('#ex1-1').on('input', function() {
    var min = parseInt($('#ex1-1 input').val()),
      old_val = $('#ex1').bootstrapSlider('getValue');
    $('#ex1').bootstrapSlider('setValue', [min, old_val[1]]);
  });

  $('#ex1-2').on('input', function() {
    var max = parseInt($('#ex1-2 input').val()),
      old_val = $('#ex1').bootstrapSlider('getValue');
    $('#ex1').bootstrapSlider('setValue', [old_val[0], max]);
  });

});